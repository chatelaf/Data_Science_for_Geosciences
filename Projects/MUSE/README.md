# deblend-exercise

A small dataset of hyperspectral cubes/mixing matrices coming from outerspace !


Galaxy indentifiant : Wavelengths associated in Angstrom
* 12  : (6500,6800)
* 26  : (7250,7550)
* 50  : (5250,5550)
* 69  : (7650,7950)
* 72  : (7600,7900)
* 79  : (6900,7200)
* 103 : (7100,7400)

Galaxy subcubes:
* 26,50  : nice continuum
* 69,72  : blended objects with lines in the same neighborhood
* 79,103 : difficult deblending!

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gh/raphbacher/deblend-exercise.git/master)



